from sqlalchemy.sql import func
from db.connector import Base, engine, sesion
from sqlalchemy import Column, String, Integer, DateTime


class Bus(Base):

    __tablename__ = 'buses'
    id = Column(Integer, primary_key=True)
    placa = Column(String(10))
    capacidad = Column(Integer)
    ruta = Column(Integer)
    pay_date = Column(DateTime(timezone=True), server_default=func.now())

    def ruta1(self):

        Base.metadata.create_all(engine)


        bus1 = Bus(placa='EF1949', capacidad=20, ruta='Penonomé')
        sesion.add(bus1)
        sesion.commit()

    def ruta2(self):

        Base.metadata.create_all(engine)

        bus1 = Bus(placa='AH5648', capacidad=30, ruta='Aguadulce')
        sesion.add(bus1)
        sesion.commit()

    def ruta3(self):
        Base.metadata.create_all(engine)

        bus1 = Bus(placa='TG7981', capacidad=40, ruta='David')
        sesion.add(bus1)
        sesion.commit()

