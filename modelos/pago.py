from sqlalchemy.sql import func
from db.connector import Base, engine, sesion
from sqlalchemy import Column, String, Integer, Float, DateTime
from modelos.Bus import Bus

class Pago(Base):
    __tablename__ = 'concretados'

    id = Column(Integer, primary_key= True)
    nombrecliente = Column(String(20))
    entidad= Column(String(20))
    saldo=Column(Float)
    version=Column(Float)
    estado= Column(String(15))
    pay_date=Column(DateTime(timezone=True), server_default=func.now())





    def menu(self):
            self.version = 1.0
            self.saldo = 0.0
            self.estado = "Paz y Salvo"
            opc = 0
            salir = 3

            while opc != salir:

                print("=======>MENÚ<=======")
                print(
                      "\n(1)USUARIO"
                      "\n(2)SALIR\n")
                opc = int(input("===Ingrese un número: "))




                if opc == 1:
                    input("Ingrese su tarjeta\n\n"
                          "(Presione Enter para ingresar la tarjeta)")
                    self.nombrecliente = str(input("Escriba su nombre para continuar: "))

                    # ===========================MENUUSUARIO
                    opc1 = 0
                    atras = 3



                    while opc1 != atras:

                        print("=======>MENÚ USUARIO<=======\n\n"
                              "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                        print("\n(1)RECARGAR"
                              "\n(2)COMPRAR PASAJE"
                              "\n(3)ATRÁS\n\n")
                        opc1 = int(input("===Ingrese un número: "))


                        if opc1 == 1:

                            opc2 = 0
                            while opc2 != 5:
                                print("=======>SELECCIONE LA ENTIDAD<=======\n\n"
                                      "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                                print("\n(1)Banco General"
                                      "\n(2)Banistmo"
                                      "\n(3)BAC"
                                      "\n(4)EFECTIVO"
                                      "\n(5)ATRÁS\n\n")
                                opc2 = int(input("===Ingrese un número: "))

                                if opc2 == 1:
                                    self.entidad = "Banco General"
                                    print("=======>BANCO GENERAL<=======\n\n"
                                          "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                                    recarga = float(input("MONTO A RECARGAR: "))
                                    self.saldo = float(self.saldo) + recarga
                                    print("\nRECARGA EXITOSA.\n"
                                          "Saldo actual: ", self.saldo)

                                elif opc2 == 2:
                                    self.entidad = "Banistmo"
                                    print("=======>BANISTMO<=======\n\n"
                                          "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                                    recarga = float(input("MONTO A RECARGAR: "))
                                    self.saldo = float(self.saldo) + recarga
                                    print("\nRECARGA EXITOSA.\n"
                                          "Saldo actual: ", self.saldo)

                                elif opc2 == 3:
                                    self.entidad = "BAC"
                                    print("=======>BAC<=======\n\n"
                                          "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                                    recarga = float(input("MONTO INGRESADO: "))
                                    self.saldo = float(self.saldo) + recarga
                                    print("\nRECARGA EXITOSA.\n"
                                          "Saldo actual: ", self.saldo)

                                elif opc2 == 4:
                                    self.entidad = "Cash"
                                    print("=======>EFECTIVO<=======\n\n"
                                         "Cliente: ", self.nombrecliente,"        Saldo: ", self.saldo,"\n\n")
                                    recarga = float(input("MONTO A RECARGAR: "))
                                    self.saldo = float(self.saldo) + recarga
                                    print("\nRECARGA EXITOSA.\n"
                                          "Saldo actual: ", self.saldo)
                                elif opc2 == 5:
                                    break


                        elif opc1 == 2:
                            # -------------------------------MenuRutas
                            bus1 = Bus()
                            opc3 = 0
                            while opc3 != 4:

                                print("=======>SELECCIONE RUTA<=======\n\n"
                                      "Saldo: ", self.saldo)
                                print("\n(1)Terminal - Penonomé ($5.00)"
                                      "\n(2)Terminal - Aguadulce ($7.00)"
                                      "\n(3)Terminal - David ($10.50)"
                                      "\n(4)ATRÁS\n\n")
                                opc3 = int(input("===Ingrese un número: "))

                                if opc3 == 1 and self.saldo >= 5.0:
                                    self.saldo = self.saldo - 5.0



                                    Base.metadata.create_all(engine)

                                    pago1 = Pago(nombrecliente=self.nombrecliente, entidad=self.entidad, saldo=self.saldo,
                                                     estado= self.estado, version=self.version)

                                    sesion.add(pago1)
                                    sesion.commit()

                                    bus1.ruta1()


                                    print("COMPRA REALIZADA CON ÉXITO: Pasaje a Penonomé\n")

                                elif opc3 == 2 and self.saldo >= 7.0:
                                        self.saldo = self.saldo - 7.0


                                        Base.metadata.create_all(engine)

                                        pago1 = Pago(nombrecliente=self.nombrecliente, entidad=self.entidad,
                                                     saldo=self.saldo,
                                                      estado=self.estado, version=self.version)
                                        sesion.add(pago1)
                                        sesion.commit()

                                        bus1.ruta2()

                                        print("COMPRA REALIZADA CON ÉXITO: Pasaje a Aguadulce\n")

                                elif opc3 == 3 and self.saldo >= 10.50:
                                        self.saldo = self.saldo - 10.50



                                        Base.metadata.create_all(engine)

                                        pago1 = Pago(nombrecliente=self.nombrecliente, entidad=self.entidad,
                                                     saldo=self.saldo,
                                                     estado=self.estado, version=self.version)
                                        sesion.add(pago1)
                                        sesion.commit()

                                        bus1.ruta3()

                                        print("COMPRA REALIZADA CON ÉXITO: Pasaje a David\n")

                                elif opc3 == 4:
                                        break
                                else:
                                    print("Saldo insuficiente.")
                                # -------------------------------
                        elif opc1 == 3:
                            break
                        # ===========================MENUUSUARIO


                elif opc == 2:
                    break














