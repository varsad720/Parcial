from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine('mysql+pymysql://sadmin:12345@localhost/pagos')
Base = declarative_base()

ManejadorSesiones = sessionmaker(bind=engine)
sesion = ManejadorSesiones()